## Pocketknife

Image ready with the `apk` packages I need for my workflow.

Feel free to add more.

### Usage

Create a Pod in your desired namespace to get a shell with all the toolz:

```bash
$ kubectl run -i -t --image=brix4dayz/swiss-army-knife --restart=Never <podname> -n <namespace>
```

Created and distributed with no responsibility :)
