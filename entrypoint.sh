#!/bin/sh

if ! whoami &> /dev/null; then
    if [ -w /etc/passwd ]; then
        echo "Creating user entry in /etc/passwd"
        echo "pocketknife:x:$(id -u):$(id -g):pocketknife user:${HOME}:/sbin/nologin" >> /etc/passwd
        echo "Created user entry in /etc/passwd"
    fi
fi

exec "$@"
